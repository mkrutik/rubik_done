#include "FaceCube.hpp"

#include <cstring>
#include <cstdlib>
#include <ctime>

#include <sstream>
#include <iterator>
#include <algorithm>
#include <utility>
#include <iostream>

#include <thread>
#include <chrono>


#define B_WHITE   "\e[0;30;47m"
#define B_BLUE    "\e[0;30;44m"
#define B_RED     "\e[0;30;41m"
#define B_GREEN   "\e[0;30;42m"
#define B_YELLOW  "\e[0;30;43m"
#define B_MAGENTA "\e[0;30;45m"
#define B_DEF     "\e[0;0m"


namespace {

typedef std::pair<unsigned, const char*> range;
const range Up(0, B_WHITE);
const range Left(1, B_RED );
const range Front(2, B_GREEN);
const range Right(3, B_YELLOW );
const range Back(4, B_MAGENTA );
const range Down(5, B_BLUE);

const std::vector<range> all_layers = {Left, Up, Front, Down, Right, Back};

// corner positions to facelet positions
Facelets cornerFacelet[CORNER_COUNT][3] = {
  { Facelets::U9, Facelets::R1, Facelets::F3},
  { Facelets::U7, Facelets::F1, Facelets::L3},
  { Facelets::U1, Facelets::L1, Facelets::B3},
  { Facelets::U3, Facelets::B1, Facelets::R3},
  { Facelets::D3, Facelets::F9, Facelets::R7},
  { Facelets::D1, Facelets::L9, Facelets::F7},
  { Facelets::D7, Facelets::B9, Facelets::L7},
  { Facelets::D9, Facelets::R9, Facelets::B7}
};

// edge positions to facelet positions
Facelets edgeFacelet[EDGE_COUNT][2] = {
  { Facelets::U6, Facelets::R2},
  { Facelets::U8, Facelets::F2},
  { Facelets::U4, Facelets::L2},
  { Facelets::U2, Facelets::B2},
  { Facelets::D6, Facelets::R8},
  { Facelets::D2, Facelets::F8},
  { Facelets::D4, Facelets::L8},
  { Facelets::D8, Facelets::B8},
  { Facelets::F6, Facelets::R4},
  { Facelets::F4, Facelets::L6},
  { Facelets::B6, Facelets::L4},
  { Facelets::B4, Facelets::R6}
};

// corner positions to facelet colors.
Color cornerColor[CORNER_COUNT][3] = {
  { Color::U, Color::R, Color::F },
  { Color::U, Color::F, Color::L },
  { Color::U, Color::L, Color::B },
  { Color::U, Color::B, Color::R },
  { Color::D, Color::F, Color::R },
  { Color::D, Color::L, Color::F },
  { Color::D, Color::B, Color::L },
  { Color::D, Color::R, Color::B }
};

// edge positions to facelet colors.
Color edgeColor[EDGE_COUNT][2] = {
  { Color::U, Color::R },
  { Color::U, Color::F },
  { Color::U, Color::L },
  { Color::U, Color::B },
  { Color::D, Color::R },
  { Color::D, Color::F },
  { Color::D, Color::L },
  { Color::D, Color::B },
  { Color::F, Color::R },
  { Color::F, Color::L },
  { Color::B, Color::L },
  { Color::B, Color::R }
};

};

bool FaceCube::is_solved(void) const {
  const Color def[54] = {Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B};

  for (unsigned i = 0; i < 54; ++i) {
    if (_face_cube[i] != def[i])
      return false;
  }
  return true;
}

FaceCube::FaceCube() {
  const Color def[54] = {Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B};
  memcpy(_face_cube, def, sizeof(def));
}

FaceCube::FaceCube(const unsigned &size) {
  const Color def[54] = {Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B};
  memcpy(_face_cube, def, sizeof(def));

  std::vector<std::string> mix = generate_mix(size);
  mixing(mix, false);
  for (auto m : mix) {
    std::cout << m << " ";
  }
  std::cout << std::endl;
}

FaceCube::FaceCube(const std::string &mix) {
  const Color def[54] = {Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::U, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::R, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::F, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::D, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::L, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B, Color::B};
  memcpy(_face_cube, def, sizeof(def));

  std::vector<std::string> mix_array = parse(mix);

  mixing(mix_array, false);
}


// FaceCube::FaceCube(const std::string &cube) {
//   for (int i = 0; i < 54; ++i) {
//     switch(cube[i]) {
//       case 'U':
//         _face_cube[i] = Color::U;
//         break;
//       case 'R':
//         _face_cube[i] = Color::R;
//         break;
//       case 'F':
//         _face_cube[i] = Color::F;
//         break;
//       case 'D':
//         _face_cube[i] = Color::D;
//         break;
//       case 'L':
//         _face_cube[i] = Color::L;
//         break;
//       case 'B':
//         _face_cube[i] = Color::B;
//         break;
//     }
//   }
// }


std::string FaceCube::to_string(void) const {
  std::string res;
  for (int i = 0; i < 54; ++i) {
   switch(_face_cube[i]) {
    case Color::U:
      res += 'U';
      break;
    case Color::R:
      res += 'R';
      break;
    case Color::F:
      res += 'F';
      break;
    case Color::D:
      res += 'D';
      break;
    case Color::L:
      res += 'L';
      break;
    case Color::B:
      res += 'B';
      break;
    }
  }

  return res;
}

std::vector<std::string>  FaceCube::parse(const std::string& mix) {
  std::stringstream ss(mix);
  std::istream_iterator<std::string> begin(ss);
  std::istream_iterator<std::string> end;

  std::vector<std::string> res(begin, end);

  for (auto move : res) {
    if (std::find(g_rotations.begin(), g_rotations.end(), move) ==  g_rotations.end()) {
      throw std::logic_error("Lexical error: unknown move - [" + move + "]");
    }
  }

  return res;
}

std::vector<std::string> FaceCube::generate_mix(unsigned size) {
  std::srand(unsigned(std::time(0)));

  std::string prev;

  std::vector<std::string> mix;
  for (unsigned i = 0; i < size; ++i) {

    std::string move;
    do {
      int index = std::rand() % 18;
      move = g_rotations[index];
    } while (!prev.empty() && move[0] == prev[0]);

    prev = move;
    mix.push_back(move);
  }

  return mix;
}

CubieCube FaceCube::to_cubie_cube(void) const {
  CubieCube res;

  signed char ori;
  Color col1;
  Color col2;

  // invalidate corners
  for (int i = 0; i < 8; i++)
    res.cp[i] = Corners::URF;

  for (int i = 0; i < 12; i++)
    res.ep[i] = Edge::UR;

  for(int i = 0; i < CORNER_COUNT; i++) {
    // get the colors of the cubie at corner i, starting with U/D
    for (ori = 0; ori < 3; ori++)
      if (_face_cube[static_cast<unsigned>(cornerFacelet[i][ori])] == Color::U ||
          _face_cube[static_cast<unsigned>(cornerFacelet[i][ori])] == Color::D)
        break;

    col1 = _face_cube[static_cast<unsigned>(cornerFacelet[i][(ori + 1) % 3])];
    col2 = _face_cube[static_cast<unsigned>(cornerFacelet[i][(ori + 2) % 3])];

    for (int j = 0; j < CORNER_COUNT; j++) {
      if (col1 == cornerColor[j][1] && col2 == cornerColor[j][2]) {
        // in cornerposition i we have cornercubie j
        res.cp[i] = static_cast<Corners>(j);
        res.co[i] = ori % 3;
        break;
      }
    }
  }

  for (int i = 0; i < EDGE_COUNT; i++) {
    for (int j = 0; j < EDGE_COUNT; j++) {
      if (_face_cube[static_cast<unsigned>(edgeFacelet[i][0])] == edgeColor[j][0]
          && _face_cube[static_cast<unsigned>(edgeFacelet[i][1])] == edgeColor[j][1]) {
        res.ep[i] = static_cast<Edge>(j);
        res.eo[i] = 0;
        break;
      }
      if (_face_cube[static_cast<unsigned>(edgeFacelet[i][0])] == edgeColor[j][1]
          && _face_cube[static_cast<unsigned>(edgeFacelet[i][1])] == edgeColor[j][0]) {
        res.ep[i] = static_cast<Edge>(j);
        res.eo[i] = 1;
        break;
      }
    }
  }
  return res;
}

using rotation_cfg = std::vector<std::pair<unsigned, unsigned> >;
using layer_rotations = std::pair<rotation_cfg, rotation_cfg >;
const std::vector<layer_rotations> layers_rotations = {
  { // Front
    { //clockwise
      {6, 44}, {7, 41}, {8, 38}, {44, 29}, {41, 28}, {38, 27}, {29, 9}, {28, 12}, {27, 15}, {9, 6}, {12, 7}, {15, 8},
      {18, 24}, {19, 21}, {20, 18}, {24, 26}, {21, 25}, {26, 20}, {25, 23}, {23, 19}
    },
    { // counter_clockwise
      {6, 9}, {7, 12}, {8, 15}, {9, 29}, {12, 28}, {15, 27}, {29, 44}, {28, 41}, {27, 38}, {44, 6}, {41, 7}, {38, 8},
      {18, 20}, {19, 23}, {20, 26}, {23, 25}, {26, 24}, {25, 21}, {24, 18}, {21, 19}
    }
  },
  { // Back
    { //clockwise
      {11, 35}, {14, 34}, {17, 33}, {35, 42}, {34, 39}, {33, 36}, {42, 0}, {39, 1}, {36, 2}, {0, 11}, {1, 14}, {2, 17},
      {45, 51}, {48, 52}, {51, 53}, {52, 50}, {53, 47}, {50, 46}, {47, 45}, {46, 48}
    },
    { // counter_clockwise
      {11, 0}, {14, 1}, {17, 2}, {35, 11}, {34, 14}, {33, 17}, {42, 35}, {39, 34}, {36, 33}, {0, 42}, {1, 39}, {2, 36},
      {45, 47}, {48, 46}, {51, 45}, {52, 48}, {53, 51}, {50, 52}, {47, 53}, {46, 50}
    },
  },
  { // Left
    { //clockwise
      {47, 33}, {50, 30}, {53, 27}, {33, 24}, {30, 21}, {27, 18}, {24, 6}, {21, 3}, {18, 0}, {6, 47}, {3, 50}, {0, 53},
      {36, 42}, {39, 43}, {42, 44}, {43, 41}, {44, 38}, {41, 37}, {38, 36}, {37, 39}
    },
    { // counter_clockwise
      {47, 6}, {50, 3}, {53, 0}, {33, 47}, {30, 50}, {27, 53}, {24, 33}, {21, 30}, {18, 27}, {6, 24}, {3, 21}, {0, 18},
      {36, 38}, {39, 37}, {42, 36}, {43, 39}, {44, 42}, {41, 43}, {38, 44}, {37, 41}
    },
  },
  { // Right
    { //clockwise
      {20, 29}, {23, 32}, {26, 35}, {29, 51}, {32, 48}, {35, 45}, {51, 2}, {48, 5}, {45, 8}, {2, 20}, {5, 23}, {8, 26},
      {9, 15}, {12, 16}, {15, 17}, {16, 14}, {17, 11}, {14, 10}, {11, 9}, {10, 12}
    },
    { // counter_clockwise
      {20, 2}, {23, 5}, {26, 8}, {29, 20}, {32, 23}, {35, 26}, {51, 29}, {48, 32}, {45, 35}, {2, 51}, {5, 48}, {8, 45},
      {9, 11}, {12, 10}, {15, 9}, {16, 12}, {17, 15}, {14, 16}, {11, 17}, {10, 14}
    },
  },
  { // Up
    { //clockwise
      {36, 18}, {37, 19}, {38, 20}, {18, 9}, {19, 10}, {20, 11}, {9, 45}, {10, 46}, {11, 47}, {45, 36}, {46, 37}, {47, 38},
      {0, 6}, {3, 7}, {6, 8}, {7, 5}, {8, 2}, {5, 1}, {2, 0}, {1, 3}
    },
    { // counter_clockwise
      {36, 45}, {37, 46}, {38, 47}, {18, 36}, {19, 37}, {20, 38}, {9, 18}, {10, 19}, {11, 20}, {45, 9}, {46, 10}, {47, 11},
      {0, 2}, {3, 1}, {6, 0}, {7, 3}, {8, 6}, {5, 7}, {2, 8}, {1, 5}
    },
  },
  { // Down
    { //clockwise
      {44, 53}, {43, 52}, {42, 51}, {53, 17}, {52, 16}, {51, 15}, {17, 26}, {16, 25}, {15, 24}, {26, 44}, {25, 43}, {24, 42},
      {27, 33}, {30, 34}, {33, 35}, {34, 32}, {35, 29}, {32, 28}, {29, 27}, {28, 30}
    },
    { // counter_clockwise
      {44, 26}, {43, 25}, {42, 24}, {53, 44}, {52, 43}, {51, 42}, {17, 53}, {16, 52}, {15, 51}, {26, 17}, {25, 16}, {24, 15},
      {27, 29}, {30, 28}, {33, 27}, {34, 30}, {35, 33}, {32, 34}, {29, 35}, {28, 32}
    }
  }
};

void FaceCube::rotate(const Layer l, const RotTypes t) {
  static Color tmp[54];
  memcpy(tmp, _face_cube, sizeof(_face_cube));


  if (t == RotTypes::Clockwise || t == RotTypes::Twice) {
    for (auto r : layers_rotations[static_cast<unsigned>(l)].first) {
      _face_cube[r.first] = tmp[r.second];
    }

    if (t == RotTypes::Twice) {
      memcpy(tmp, _face_cube, 54);

      for (auto r : layers_rotations[static_cast<unsigned>(l)].first) {
        _face_cube[r.first] = tmp[r.second];
      }
    }

  } else if (t == RotTypes::CounterClockwise) {

    for (auto r : layers_rotations[static_cast<unsigned>(l)].second) {
      _face_cube[r.first] = tmp[r.second];
    }
  }
}

void FaceCube::mixing(const std::vector<std::string> &mix, bool print) {
  for (const std::string &move : mix) {
    if (move == F)
      rotate(Layer::Front, RotTypes::Clockwise);
    else if (move == Fb)
      rotate(Layer::Front, RotTypes::CounterClockwise);
    else if (move == F2)
      rotate(Layer::Front, RotTypes::Twice);

    else if (move == B)
      rotate(Layer::Back, RotTypes::Clockwise);
    else if (move == Bb)
      rotate(Layer::Back, RotTypes::CounterClockwise);
    else if (move == B2)
      rotate(Layer::Back, RotTypes::Twice);

    else if (move == L)
      rotate(Layer::Left, RotTypes::Clockwise);
    else if (move == Lb)
      rotate(Layer::Left, RotTypes::CounterClockwise);
    else if (move == L2)
      rotate(Layer::Left, RotTypes::Twice);

    else if (move == R)
      rotate(Layer::Right, RotTypes::Clockwise);
    else if (move == Rb)
      rotate(Layer::Right, RotTypes::CounterClockwise);
    else if (move == R2)
      rotate(Layer::Right, RotTypes::Twice);

    else if (move == U)
      rotate(Layer::Up, RotTypes::Clockwise);
    else if (move == Ub)
      rotate(Layer::Up, RotTypes::CounterClockwise);
    else if (move == U2)
      rotate(Layer::Up, RotTypes::Twice);

    else if (move == D)
      rotate(Layer::Down, RotTypes::Clockwise);
    else if (move == Db)
      rotate(Layer::Down, RotTypes::CounterClockwise);
    else if (move == D2)
      rotate(Layer::Down, RotTypes::Twice);
    else
      throw std::runtime_error("Unknown move");

    if (print) {
      std::cout << B_GREEN << "Move " <<  move << B_DEF;
      this->print();
      std::this_thread::sleep_for(std::chrono::seconds(1));
    }
  }
}


void FaceCube::print(void) const {
  for (unsigned i = 0; i <= 8; ++i) {
    for (auto l : all_layers) {
      unsigned val = static_cast<unsigned>(_face_cube[i]);
      if (val == l.first) {
        if (i == 0 || i % 3 == 0)
          std::cout << std::endl << std::endl << "         ";

        std::cout << l.second << "  " << B_DEF << " ";
      }
    }
  }
  std::cout << std::endl << std::endl;

  const unsigned midle[36] = {36, 37, 38, 18, 19, 20,  9, 10, 11, 45, 46, 47,
                              39, 40, 41, 21, 22, 23, 12, 13, 14, 48, 49, 50,
                              42, 43, 44, 24, 25, 26, 15, 16, 17, 51, 52, 53 };

  for (unsigned i = 0; i < 36; ++i) {
    for (auto l : all_layers) {
      unsigned val = static_cast<unsigned>(_face_cube[midle[i]]);
      if (val == l.first) {
        std::cout << l.second << "  " << B_DEF;

        if ((i + 1) % 12 == 0 && i != 35)
          std::cout << std::endl << std::endl;
        else
          std::cout << " ";
      }
    }
  }

  for (unsigned i = 27; i <= 35; ++i) {
    for (auto l : all_layers) {
      unsigned val = static_cast<unsigned>(_face_cube[i]);
      if (val == l.first) {
        if (i == 45 || i % 3 == 0)
          std::cout << std::endl << std::endl << "         ";
        std::cout << l.second << "  " << B_DEF << " ";
      }
    }
  }

  std::cout << std::endl;
}