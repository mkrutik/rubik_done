#ifndef SOLVER_HPP
#define SOLVER_HPP

#include "header.h"
#include "CubieCube.hpp"
#include "FaceCube.hpp"
#include "CordCube.hpp"

#include <string>

class Solver {
public:
  std::string solution(const FaceCube &face_cube, int max_depth);

private:
  std::string to_string(int length) const;

  // U,D,R2,F2,L2 and B2 are allowed.
  int total_depth(void);

private:
  FaceCube  _face_cube;
  CubieCube _cubie_cube;
  CordCube  _cord_cube;
  CordCube::cord_cube_state_t _cord_cube_state;

  int  _max_depth;
  int  _depth_phase1;
  int  _total_depth;

  int ax[31]; // The axis of the move
  int po[31]; // The power of the move

  // phase1 coordinates
  int flip[31];
  int twist[31];
  int slice[31];

  // phase2 coordinates
  int parity[31];
  int URFtoDLF[31];
  int FRtoBR[31];
  int URtoUL[31];
  int UBtoDF[31];
  int URtoDF[31];

  // IDA* distance do goal estimations
  int minDistPhase1[31];
  int minDistPhase2[31];

};


#endif // SOLVER_HPP
