#ifndef HEADER_H
#define HEADER_H

#include <vector>
#include <string>
#include <optional>

#define DEF     "\e[0;0m"
#define CYAN    "\e[0;35m"
#define RED     "\e[0;31m"

const std::string F   = "F";
const std::string Fb  = "F'";
const std::string F2  = "F2";
const std::string B   = "B";
const std::string Bb  = "B'";
const std::string B2  = "B2";
const std::string R   = "R";
const std::string Rb  = "R'";
const std::string R2  = "R2";
const std::string L   = "L";
const std::string Lb  = "L'";
const std::string L2  = "L2";
const std::string U   = "U";
const std::string Ub  = "U'";
const std::string U2  = "U2";
const std::string D   = "D";
const std::string Db  = "D'";
const std::string D2  = "D2";

// allowed rotation
const std::vector<std::string> g_rotations{
  F, Fb, F2,
  B, Bb, B2,
  R, Rb, R2,
  L, Lb, L2,
  U, Ub, U2,
  D, Db, D2
};

//             U1 U2 U3
//             U4 U5 U6
//             U7 U8 U9
//
//   L1 L2 L3  F1 F2 F3  R1 R2 F3  B1 B2 B3
//   L4 L5 L6  F4 F5 F6  R4 R5 R6  B4 B5 B6
//   L7 L8 L9  F7 F8 F9  R7 R8 R9  B7 B8 B9
//
//             D1 D2 D3
//             D4 D5 D6
//             D7 D8 D9
const unsigned Facelets_count = 54;
enum class Facelets : char {
  U1, U2, U3, U4, U5, U6, U7, U8, U9, R1, R2, R3, R4, R5, R6, R7, R8, R9, F1, F2, F3, F4, F5, F6, F7, F8, F9, D1, D2, D3, D4, D5, D6, D7, D8, D9, L1, L2, L3, L4, L5, L6, L7, L8, L9, B1, B2, B3, B4, B5, B6, B7, B8, B9
};

const int Color_COUNT = 6;
enum class Color : char {
  U, R, F, D, L, B
};

const int CORNER_COUNT = 8;
enum class Corners : char {
  URF, UFL, ULB, UBR, DFR, DLF, DBL, DRB
};

const int EDGE_COUNT = 12;
enum class Edge : char {
  UR, UF, UL, UB, DR, DF, DL, DB, FR, FL, BL, BR
};

const unsigned N_MOVE     = 18;
const unsigned N_TWIST    = 2187;
const unsigned N_FLIP     = 2048;
const unsigned N_SLICE1   = 495;
const unsigned N_SLICE2   = 24;
const unsigned N_PARITY   = 2;

const unsigned N_URFtoDLF = 20160;
const unsigned N_URFtoDLB = 40320;

const unsigned N_FRtoBR   = 11880;
const unsigned N_URtoUL   = 1320;
const unsigned N_UBtoDF   = 1320;
const unsigned N_URtoDF   = 20160;
const unsigned N_URtoBR   = 479001600;


typedef struct {
  std::optional<std::string>  mix;
  std::optional<unsigned>     g_size;
  bool                        visual;
  // algorithm
} ProgramOptions;

ProgramOptions parse_arguments(int argc, const char *argv[]);


#endif // HEADER_H