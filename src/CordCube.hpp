#ifndef CORDCUBE_HPP
#define CORDCUBE_HPP

#include "CubieCube.hpp"
#include <string>

class CordCube {
public:
  enum class PruningTable : unsigned {
    URFtoDLF,
    URtoDF,
    Twist,
    Flip
  };

  typedef struct cord_cube_state {
    short twist;
    short flip;
    short parity;
    short FRtoBR;
    short URFtoDLF;
    short URtoUL;
    short UBtoDF;
    int   URtoDF;
  } cord_cube_state_t;


  CordCube();
  CordCube(const CordCube& cp) = delete;

  signed char get_pruning(PruningTable table, unsigned index);
  cord_cube_state_t get_coordcube(const CubieCube &cubiecube);

private:
  void init_pruning(void);
  void set_pruning(PruningTable table, unsigned index, signed char value);
};

extern short twistMove[N_TWIST][N_MOVE];
extern short flipMove[N_FLIP][N_MOVE];
extern short parityMove[2][18];

extern short FRtoBR_Move[N_FRtoBR][N_MOVE];
extern short URFtoDLF_Move[N_URFtoDLF][N_MOVE];
extern short URtoDF_Move[N_URtoDF][N_MOVE];


extern short URtoUL_Move[N_URtoUL][N_MOVE];
extern short UBtoDF_Move[N_UBtoDF][N_MOVE];
extern short MergeURtoULandUBtoDF[336][336];

extern signed char Slice_URFtoDLF_Parity_Prun[N_SLICE2 * N_URFtoDLF * N_PARITY / 2];
extern signed char Slice_URtoDF_Parity_Prun[N_SLICE2 * N_URtoDF * N_PARITY / 2];
extern signed char Slice_Twist_Prun[N_SLICE1 * N_TWIST / 2 + 1];
extern signed char Slice_Flip_Prun[N_SLICE1 * N_FLIP / 2];

#endif // CORDCUBE_HPP