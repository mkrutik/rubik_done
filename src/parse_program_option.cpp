#include "header.h"
#include <boost/program_options.hpp>
#include <iostream>


namespace po = boost::program_options;


void conflicting_options(const boost::program_options::variables_map & vm,
                         const std::string & opt1, const std::string & opt2) {
  if (vm.count(opt1) && vm.count(opt2))
    throw std::logic_error(std::string("Conflicting options '") +
                          opt1 + "' and '" + opt2 + "'.");
}

ProgramOptions parse_arguments(int argc, const char *argv[]) {
  ProgramOptions options;
  options.visual = false;
  unsigned g_size;
  std::string mix;

  try {
    po::options_description gen_op{"Rubik options"};
    gen_op.add_options()
      ("mix", po::value<std::string>(&mix), "Moves for mixing rubik")
      ("g_size", po::value<unsigned>(&g_size), "Number of moves for mixing generated rubik")
      ("visual,v", "Print rubik representation")
      ("help,h", "Help screen");

    po::positional_options_description positionalDescription;
    positionalDescription.add( "mix", -1 );

    po::variables_map opts;
    po::parsed_options parsed = po::command_line_parser(argc, argv)
        .options(gen_op)
        .positional(positionalDescription)
        .run( );
    po::store(parsed, opts);
    po::notify(opts);

    conflicting_options(opts, "mix", "g_size");

    if (opts.count("help")) {
      std::cout << CYAN << gen_op << DEF << std::endl;
      exit(1);
    }

    if (opts.count("g_size") == 1)
      options.g_size.emplace(g_size);

    if (opts.count("mix") == 1)
      options.mix.emplace(mix);

    if (opts.count("visual") == 1)
      options.visual = true;
  }
  catch (po::error &ex) {
    throw std::logic_error(ex.what());
  }

  return options;
}
