#include "Solver.hpp"

#include <string>
#include <iostream>

void print_error(std::string msg) {
  std::cout << RED
            << msg
            << DEF
            << std::endl;
}

int main(int argc, char const *argv[]) {
  try {
    ProgramOptions po = parse_arguments(argc, argv);
    FaceCube face_cube;

    std::vector<std::string> mix;
    if (po.mix.has_value()) {
      if ((*po.mix).empty()) {
        print_error("Empty mix");
        return (-1);
      }

      std::string mix_s(*po.mix);
      face_cube = FaceCube(mix_s);

    } else if (po.g_size.has_value()) {
      face_cube = FaceCube(*po.g_size);

    } else {
      std::cout << CYAN << "Generated mix with 30 moves ... " << DEF << std::endl;
      face_cube = FaceCube(30);
    }

    if (face_cube.is_solved()) {
      print_error("Cube already solved");
      return (-1);
    }

    if (po.visual) {
      if (po.mix.has_value()) {
        std::cout << CYAN << "MIX : ";
        for (auto move : mix) {
          std::cout << move << " ";
        }
        std::cout << std::endl;
      }

      std::cout << CYAN << "Initial cube state" << DEF;
      face_cube.print();
    }

    Solver solver;
    std::string path = solver.solution(face_cube, 20); // 20 moves


    if (po.visual) {
      face_cube.mixing(face_cube.parse(path), true);
    } else
      std::cout << path << std::endl;

  } catch(std::exception &e) {
    print_error(e.what());
    return (-1);
  }

  return 0;
}