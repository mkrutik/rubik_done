#ifndef FACECUBE_HPP
#define FACECUBE_HPP

#include "header.h"
#include "CubieCube.hpp"

class FaceCube {
public:
  FaceCube();
  FaceCube(const unsigned &size);
  FaceCube(const std::string &mix);
  // FaceCube(const std::string &cube_string);

  std::string to_string(void) const;
  CubieCube to_cubie_cube(void) const;

  bool is_solved(void) const;

  std::vector<std::string>  parse(const std::string& mix);
  void  mixing(const std::vector<std::string> &mix, bool print);
  void print() const;


private:
  std::vector<std::string>  generate_mix(unsigned size);

  enum class RotTypes : char {
    Clockwise        = 0,
    CounterClockwise = 1,
    Twice            = 2
  };

  enum class Layer : char {
    Unknown = -1,
    Front   = 0,
    Back    = 1,
    Left    = 2,
    Right   = 3,
    Up      = 4,
    Down    = 5
  };

  void rotate(const Layer l, const RotTypes t);


private:

  Color _face_cube[54];
};

#endif // FACECUBE_HPP
