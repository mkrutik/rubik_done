#include "Solver.hpp"

#include <algorithm>

std::string Solver::to_string(int length) const
{
  std::string res;

  for (int i = 0; i < length; i++)
  {
    switch (this->ax[i])
    {
    case 0:
      res += 'U';
      break;
    case 1:
      res += 'R';
      break;
    case 2:
      res += 'F';
      break;
    case 3:
      res += 'D';
      break;
    case 4:
      res += 'L';
      break;
    case 5:
      res += 'B';
      break;
    }

    switch (this->po[i])
    {
    case 1:
      res += ' ';
      break;
    case 2:
      res += '2';
      res += ' ';
      break;
    case 3:
      res += '\'';
      res += ' ';
      break;
    }
  }

  return res;
}

std::string Solver::solution(const FaceCube &face_cube, int max_depth)
{
  int s;
  int mv, n;
  int busy;

  _face_cube = face_cube;
  _cubie_cube = _face_cube.to_cubie_cube();
  _cord_cube_state = _cord_cube.get_coordcube(_cubie_cube);

  this->po[0] = 0;
  this->ax[0] = 0;
  this->flip[0] = _cord_cube_state.flip;
  this->twist[0] = _cord_cube_state.twist;
  this->parity[0] = _cord_cube_state.parity;
  this->slice[0] = _cord_cube_state.FRtoBR / 24;
  this->URFtoDLF[0] = _cord_cube_state.URFtoDLF;
  this->FRtoBR[0] = _cord_cube_state.FRtoBR;
  this->URtoUL[0] = _cord_cube_state.URtoUL;
  this->UBtoDF[0] = _cord_cube_state.UBtoDF;

  this->minDistPhase1[1] = 1; // else failure for depth=1, n=0
  mv = 0;
  n = 0;
  busy = 0;
  _depth_phase1 = 1;
  _max_depth = max_depth;


  // +++++++++++++++++++ Main loop ++++++++++++++++++++++++++++++++++++++++++
  do
  {
    do
    {
      if ((_depth_phase1 - n > this->minDistPhase1[n + 1]) && !busy)
      {

        if (this->ax[n] == 0 || this->ax[n] == 3) // Initialize next move
          this->ax[++n] = 1;
        else
          this->ax[++n] = 0;
        this->po[n] = 1;
      }
      else if (++this->po[n] > 3)
      {
        do
        { // increment axis
          if (++this->ax[n] > 5)
          {
            if (n == 0)
            {
              if (_depth_phase1 >= _max_depth)
                return NULL;
              else
              {
                _depth_phase1++;
                this->ax[n] = 0;
                this->po[n] = 1;
                busy = 0;
                break;
              }
            }
            else
            {
              n--;
              busy = 1;
              break;
            }
          }
          else
          {
            this->po[n] = 1;
            busy = 0;
          }
        } while (n != 0 && (this->ax[n - 1] == this->ax[n] || this->ax[n - 1] - 3 == this->ax[n]));
      }
      else
        busy = 0;
    } while (busy);

    // +++++++++++++ compute new coordinates and new minDistPhase1 ++++++++++
    // if minDistPhase1 =0, the H subgroup is reached
    mv = 3 * this->ax[n] + this->po[n] - 1;
    this->flip[n + 1] = flipMove[this->flip[n]][mv];
    this->twist[n + 1] = twistMove[this->twist[n]][mv];
    this->slice[n + 1] = FRtoBR_Move[this->slice[n] * 24][mv] / 24;
    this->minDistPhase1[n + 1] = std::max(
        _cord_cube.get_pruning(CordCube::PruningTable::Flip, N_SLICE1 * this->flip[n + 1] + this->slice[n + 1]),
        _cord_cube.get_pruning(CordCube::PruningTable::Twist, N_SLICE1 * this->twist[n + 1] + this->slice[n + 1]));
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // System.out.format("%d %d\n", n, _depth_phase1);
    if (this->minDistPhase1[n + 1] == 0 && n >= _depth_phase1 - 5)
    {
      this->minDistPhase1[n + 1] = 10; // instead of 10 any value >5 is possible
      if (n == _depth_phase1 - 1 && (s = total_depth()) >= 0)
      {
        if (s == _depth_phase1 || (this->ax[_depth_phase1 - 1] != this->ax[_depth_phase1] && this->ax[_depth_phase1 - 1] != this->ax[_depth_phase1] + 3))
        {
          std::string res = to_string(s);
          return res;
        }
      }
    }
  } while (1);
}

int Solver::total_depth()
{
  int mv = 0, d1 = 0, d2 = 0, i;
  int _max_depthPhase2 = std::min(10, _max_depth - _depth_phase1); // Allow only max 10 moves in phase2
  int depthPhase2;
  int n;
  int busy;
  for (i = 0; i < _depth_phase1; i++)
  {
    mv = 3 * this->ax[i] + this->po[i] - 1;
    // System.out.format("%d %d %d %d\n", i, mv, ax[i], po[i]);
    this->URFtoDLF[i + 1] = URFtoDLF_Move[this->URFtoDLF[i]][mv];
    this->FRtoBR[i + 1] = FRtoBR_Move[this->FRtoBR[i]][mv];
    this->parity[i + 1] = parityMove[this->parity[i]][mv];
  }

  if ((d1 = _cord_cube.get_pruning(CordCube::PruningTable::URFtoDLF,
                       (N_SLICE2 * this->URFtoDLF[_depth_phase1] + this->FRtoBR[_depth_phase1]) * 2 + this->parity[_depth_phase1])) > _max_depthPhase2)
    return -1;

  for (i = 0; i < _depth_phase1; i++)
  {
    mv = 3 * this->ax[i] + this->po[i] - 1;
    this->URtoUL[i + 1] = URtoUL_Move[this->URtoUL[i]][mv];
    this->UBtoDF[i + 1] = UBtoDF_Move[this->UBtoDF[i]][mv];
  }
  this->URtoDF[_depth_phase1] = MergeURtoULandUBtoDF[this->URtoUL[_depth_phase1]][this->UBtoDF[_depth_phase1]];

  if ((d2 = _cord_cube.get_pruning(CordCube::PruningTable::URtoDF,
                       (N_SLICE2 * this->URtoDF[_depth_phase1] + this->FRtoBR[_depth_phase1]) * 2 + this->parity[_depth_phase1])) > _max_depthPhase2)
    return -1;

  if ((this->minDistPhase2[_depth_phase1] = std::max(d1, d2)) == 0) // already solved
    return _depth_phase1;

  // now set up search

  depthPhase2 = 1;
  n = _depth_phase1;
  busy = 0;
  this->po[_depth_phase1] = 0;
  this->ax[_depth_phase1] = 0;
  this->minDistPhase2[n + 1] = 1; // else failure for depthPhase2=1, n=0
  // +++++++++++++++++++ end initialization +++++++++++++++++++++++++++++++++
  do
  {
    do
    {
      if ((_depth_phase1 + depthPhase2 - n > this->minDistPhase2[n + 1]) && !busy)
      {

        if (this->ax[n] == 0 || this->ax[n] == 3) // Initialize next move
        {
          this->ax[++n] = 1;
          this->po[n] = 2;
        }
        else
        {
          this->ax[++n] = 0;
          this->po[n] = 1;
        }
      }
      else if ((this->ax[n] == 0 || this->ax[n] == 3) ? (++this->po[n] > 3) : ((this->po[n] = this->po[n] + 2) > 3))
      {
        do
        { // increment axis
          if (++this->ax[n] > 5)
          {
            if (n == _depth_phase1)
            {
              if (depthPhase2 >= _max_depthPhase2)
                return -1;
              else
              {
                depthPhase2++;
                this->ax[n] = 0;
                this->po[n] = 1;
                busy = 0;
                break;
              }
            }
            else
            {
              n--;
              busy = 1;
              break;
            }
          }
          else
          {
            if (this->ax[n] == 0 || this->ax[n] == 3)
              this->po[n] = 1;
            else
              this->po[n] = 2;
            busy = 0;
          }
        } while (n != _depth_phase1 && (this->ax[n - 1] == this->ax[n] || this->ax[n - 1] - 3 == this->ax[n]));
      }
      else
        busy = 0;
    } while (busy);
    // +++++++++++++ compute new coordinates and new minDist ++++++++++
    mv = 3 * this->ax[n] + this->po[n] - 1;

    this->URFtoDLF[n + 1] = URFtoDLF_Move[this->URFtoDLF[n]][mv];
    this->FRtoBR[n + 1] = FRtoBR_Move[this->FRtoBR[n]][mv];
    this->parity[n + 1] = parityMove[this->parity[n]][mv];
    this->URtoDF[n + 1] = URtoDF_Move[this->URtoDF[n]][mv];

    this->minDistPhase2[n + 1] = std::max(_cord_cube.get_pruning(CordCube::PruningTable::URtoDF, (N_SLICE2 * this->URtoDF[n + 1] + this->FRtoBR[n + 1]) * 2 + this->parity[n + 1]), _cord_cube.get_pruning(CordCube::PruningTable::URFtoDLF, (N_SLICE2 * this->URFtoDLF[n + 1] + this->FRtoBR[n + 1]) * 2 + this->parity[n + 1]));
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  } while (this->minDistPhase2[n + 1] != 0);
  return _depth_phase1 + depthPhase2;
}