#ifndef CUBIECUBE_HPP
#define CUBIECUBE_HPP

#include "header.h"

class CubieCube {
public:

  static CubieCube* get_default(void);
  static CubieCube  get_cubiecube(void);

  void  corner_multiply(const CubieCube &an);
  void  edge_multiply(const CubieCube &an);
  void  multiply(const CubieCube &an);

  short get_twist(void) const;
  void  set_twist(short twist);

  short get_flip(void) const;
  void  set_flip(short flip);

  short corner_parity(void) const;
  short edge_parity(void) const;

  short get_FRtoBR(void) const;
  void  set_FRtoBR(short index);

  short get_URFtoDLF(void) const;
  void  set_URFtoDLF(short index);

  int   get_URtoDF(void) const;
  void  set_URtoDF(int index);

  short get_URtoUL(void) const;
  void  set_URtoUL(short index);

  short get_UBtoDF(void) const;
  void  set_UBtoDF(short index);

  int   get_URFtoDLB(void) const;
  void  set_URFtoDLB(int index);

  int   get_URtoBR(void) const;
  void  set_URtoBR(int index);


  static int get_URtoDF_standalone(short idx1, short idx2);

private:
  void  rotateLeft_corner(Corners *arr, int l, int r) const;
  void  rotateRight_corner(Corners *arr, int l, int r) const;
  void  rotateLeft_edge(Edge *arr, int l, int r) const;
  void  rotateRight_edge(Edge *arr, int l, int r) const;
  int   Cnk(int n, int k) const;

public:
  Corners cp[CORNER_COUNT]; //permutation
  signed char co[CORNER_COUNT]; // orientation

  Edge ep[EDGE_COUNT]; // permutation
  signed char eo[EDGE_COUNT]; // orientation
};

#endif // CUBIECUBE_HPP