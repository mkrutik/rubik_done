#include "CordCube.hpp"

#include <vector>

std::vector<signed char*> all_tables = {
  Slice_URFtoDLF_Parity_Prun,
  Slice_URtoDF_Parity_Prun,
  Slice_Twist_Prun,
  Slice_Flip_Prun
};

CordCube::CordCube() {
  init_pruning();
}

void CordCube::set_pruning(PruningTable table, unsigned index, signed char value) {
  if ((index & 1) == 0)
    all_tables[static_cast<unsigned>(table)][index / 2] &= 0xf0 | value;
  else
    all_tables[static_cast<unsigned>(table)][index / 2] &= 0x0f | (value << 4);
}

signed char CordCube::get_pruning(PruningTable table, unsigned index) {
  signed char res;

  if ((index & 1) == 0)
    res = (all_tables[static_cast<unsigned>(table)][index / 2] & 0x0f);
  else
    res = ((all_tables[static_cast<unsigned>(table)][index / 2] >> 4) & 0x0f);

  return res;
}

CordCube::cord_cube_state_t CordCube::get_coordcube(const CubieCube &cubiecube) {
  cord_cube_state_t result;

  result.twist    = cubiecube.get_twist();
  result.flip     = cubiecube.get_flip();
  result.parity   = cubiecube.corner_parity();
  result.FRtoBR   = cubiecube.get_FRtoBR();
  result.URFtoDLF = cubiecube.get_URFtoDLF();
  result.URtoUL   = cubiecube.get_URtoUL();
  result.UBtoDF   = cubiecube.get_UBtoDF();
  result.URtoDF   = cubiecube.get_URtoDF();
  return result;
}

void CordCube::init_pruning(void) {
  CubieCube a;
  CubieCube *moveCube = CubieCube::get_default();

  { // twistMove
    a = CubieCube::get_cubiecube();
    for (unsigned i = 0; i < N_TWIST; i++) {
      a.set_twist(i);
      for (unsigned j = 0; j < 6; j++) {
        for (unsigned k = 0; k < 3; k++) {
          a.corner_multiply(moveCube[j]);
          twistMove[i][3 * j + k] = a.get_twist();
        }
        a.corner_multiply(moveCube[j]);
      }
    }
  }

  { // flipMove
    a = CubieCube::get_cubiecube();
    for (unsigned i = 0; i < N_FLIP; i++) {
      a.set_flip(i);
      for (int j = 0; j < 6; j++) {
        for (int k = 0; k < 3; k++) {
          a.edge_multiply(moveCube[j]);
          flipMove[i][3 * j + k] = a.get_flip();
        }
        a.edge_multiply(moveCube[j]);
      }
    }
  }

  { // FRtoBR_Move
    a = CubieCube::get_cubiecube();
    for (unsigned i = 0; i < N_FRtoBR; i++) {
      a.set_FRtoBR(i);
      for (int j = 0; j < 6; j++) {
        for (int k = 0; k < 3; k++) {
          a.edge_multiply(moveCube[j]);
          FRtoBR_Move[i][3 * j + k] = a.get_FRtoBR();
        }
        a.edge_multiply(moveCube[j]);
      }
    }
  }

  { // URFtoDLF_Move
    a = CubieCube::get_cubiecube();
    for (unsigned i = 0; i < N_URFtoDLF; i++) {
      a.set_URFtoDLF(i);
      for (int j = 0; j < 6; j++) {
        for (int k = 0; k < 3; k++) {
          a.corner_multiply(moveCube[j]);
          URFtoDLF_Move[i][3 * j + k] = a.get_URFtoDLF();
        }
        a.corner_multiply(moveCube[j]);
      }
    }
  }

  { // URtoDF_Move
    a = CubieCube::get_cubiecube();
    for (unsigned i = 0; i < N_URtoDF; i++) {
      a.set_URtoDF(i);
      for (int j = 0; j < 6; j++) {
        for (int k = 0; k < 3; k++) {
          a.edge_multiply(moveCube[j]);
          URtoDF_Move[i][3 * j + k] = static_cast<short>(a.get_URtoDF());
          // Table values are only valid for phase 2 moves!
          // For phase 1 moves, casting to short is not possible.
        }
        a.edge_multiply(moveCube[j]);
      }
    }
  }

    { // URtoUL_Move
      a = CubieCube::get_cubiecube();
      for (unsigned i = 0; i < N_URtoUL; i++) {
        a.set_URtoUL(i);
        for (int j = 0; j < 6; j++) {
          for (int k = 0; k < 3; k++) {
            a.edge_multiply(moveCube[j]);
            URtoUL_Move[i][3 * j + k] = a.get_URtoUL();
          }
          a.edge_multiply(moveCube[j]);
        }
      }
    }

    { // UBtoDF_Move
      a = CubieCube::get_cubiecube();
      for (unsigned i = 0; i < N_UBtoDF; i++) {
        a.set_UBtoDF(i);
        for (int j = 0; j < 6; j++) {
          for (int k = 0; k < 3; k++) {
            a.edge_multiply(moveCube[j]);
            UBtoDF_Move[i][3 * j + k] = a.get_UBtoDF();
          }
          a.edge_multiply(moveCube[j]);
        }
      }
    }


    { // MergeURtoULandUBtoDF
      short uRtoUL, uBtoDF;
      for (uRtoUL = 0; uRtoUL < 336; uRtoUL++) {
        for (uBtoDF = 0; uBtoDF < 336; uBtoDF++) {
          MergeURtoULandUBtoDF[uRtoUL][uBtoDF] = static_cast<short>(CubieCube::get_URtoDF_standalone(uRtoUL, uBtoDF));
        }
      }
    }

    { // Slice_URFtoDLF_Parity_Prun
      int depth = 0;
      int done = 1;
      for (unsigned i = 0; i < N_SLICE2 * N_URFtoDLF * N_PARITY / 2; i++)
          Slice_URFtoDLF_Parity_Prun[i] = -1;

      set_pruning(PruningTable::URFtoDLF, 0, 0);
      while (done != N_SLICE2 * N_URFtoDLF * N_PARITY) {
        for (unsigned i = 0; i < N_SLICE2 * N_URFtoDLF * N_PARITY; i++) {
          int parity = i % 2;
          int URFtoDLF = (i / 2) / N_SLICE2;
          int slice = (i / 2) % N_SLICE2;
          if (get_pruning(PruningTable::URFtoDLF, i) == depth) {
            for (int j = 0; j < 18; j++) {
              int newSlice;
              int newURFtoDLF;
              int newParity;
              switch (j) {
              case 3:
              case 5:
              case 6:
              case 8:
              case 12:
              case 14:
              case 15:
              case 17:
                  continue;
              default:
                newSlice = FRtoBR_Move[slice][j];
                newURFtoDLF = URFtoDLF_Move[URFtoDLF][j];
                newParity = parityMove[parity][j];
                if (get_pruning(PruningTable::URFtoDLF, (N_SLICE2 * newURFtoDLF + newSlice) * 2 + newParity) == 0x0f) {
                    set_pruning(PruningTable::URFtoDLF, (N_SLICE2 * newURFtoDLF + newSlice) * 2 + newParity,
                            (signed char) (depth + 1));
                  done++;
                }
              }
            }
          }
        }
        depth++;
      }
    }


    { // Slice_URtoDF_Parity_Prun
      int depth = 0;
      int done = 1;
      for (unsigned i = 0; i < N_SLICE2 * N_URtoDF * N_PARITY / 2; i++)
        Slice_URtoDF_Parity_Prun[i] = -1;

      set_pruning(PruningTable::URtoDF, 0, 0);
      while (done != N_SLICE2 * N_URtoDF * N_PARITY) {
        for (unsigned i = 0; i < N_SLICE2 * N_URtoDF * N_PARITY; i++) {
          int parity = i % 2;
          int URtoDF = (i / 2) / N_SLICE2;
          int slice = (i / 2) % N_SLICE2;
          if (get_pruning(PruningTable::URtoDF, i) == depth) {
            for (int j = 0; j < 18; j++) {
              int newSlice;
              int newURtoDF;
              int newParity;
              switch (j) {
              case 3:
              case 5:
              case 6:
              case 8:
              case 12:
              case 14:
              case 15:
              case 17:
                  continue;
              default:
                newSlice = FRtoBR_Move[slice][j];
                newURtoDF = URtoDF_Move[URtoDF][j];
                newParity = parityMove[parity][j];
                if (get_pruning(PruningTable::URtoDF, (N_SLICE2 * newURtoDF + newSlice) * 2 + newParity) == 0x0f) {
                    set_pruning(PruningTable::URtoDF, (N_SLICE2 * newURtoDF + newSlice) * 2 + newParity,
                            (signed char) (depth + 1));
                    done++;
                }
              }
            }
          }
        }
        depth++;
      }
    }

    { // Slice_Twist_Prun
      int depth = 0;
      int done = 1;
      for (unsigned i = 0; i < N_SLICE1 * N_TWIST / 2 + 1; i++)
          Slice_Twist_Prun[i] = -1;

      set_pruning(PruningTable::Twist, 0, 0);
      while (done != N_SLICE1 * N_TWIST) {
        for (unsigned i = 0; i < N_SLICE1 * N_TWIST; i++) {
          int twist = i / N_SLICE1, slice = i % N_SLICE1;
          if (get_pruning(PruningTable::Twist, i) == depth) {
            for (unsigned j = 0; j < 18; j++) {
              int newSlice = FRtoBR_Move[slice * 24][j] / 24;
              int newTwist = twistMove[twist][j];
              if (get_pruning(PruningTable::Twist, N_SLICE1 * newTwist + newSlice) == 0x0f) {
                  set_pruning(PruningTable::Twist, N_SLICE1 * newTwist + newSlice, (signed char) (depth + 1));
                  done++;
              }
            }
          }
        }
        depth++;
      }
    }

  { // Slice_Flip_Prun
    int depth = 0;
    int done = 1;
    for (unsigned i = 0; i < N_SLICE1 * N_FLIP / 2; i++)
        Slice_Flip_Prun[i] = -1;

    set_pruning(PruningTable::Flip, 0, 0);
    while (done != N_SLICE1 * N_FLIP) {
      for (unsigned i = 0; i < N_SLICE1 * N_FLIP; i++) {
        int flip = i / N_SLICE1, slice = i % N_SLICE1;
        if (get_pruning(PruningTable::Flip, i) == depth) {
          for (int j = 0; j < 18; j++) {
            int newSlice = FRtoBR_Move[slice * 24][j] / 24;
            int newFlip = flipMove[flip][j];
            if (get_pruning(PruningTable::Flip, N_SLICE1 * newFlip + newSlice) == 0x0f) {
                set_pruning(PruningTable::Flip, N_SLICE1 * newFlip + newSlice, (signed char) (depth + 1));
                done++;
            }
          }
        }
      }
      depth++;
    }
  }
}