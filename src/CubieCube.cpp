#include "CubieCube.hpp"

#include <cstring>

short twistMove[N_TWIST][N_MOVE];
short flipMove[N_FLIP][N_MOVE];
short parityMove[2][18] = {
    { 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1 },
    { 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0 }
};

short FRtoBR_Move[N_FRtoBR][N_MOVE];
short URFtoDLF_Move[N_URFtoDLF][N_MOVE];
short URtoDF_Move[N_URtoDF][N_MOVE];
short URtoUL_Move[N_URtoUL][N_MOVE];
short UBtoDF_Move[N_UBtoDF][N_MOVE];
short MergeURtoULandUBtoDF[336][336];
signed char Slice_URFtoDLF_Parity_Prun[N_SLICE2 * N_URFtoDLF * N_PARITY / 2];
signed char Slice_URtoDF_Parity_Prun[N_SLICE2 * N_URtoDF * N_PARITY / 2];
signed char Slice_Twist_Prun[N_SLICE1 * N_TWIST / 2 + 1];
signed char Slice_Flip_Prun[N_SLICE1 * N_FLIP / 2];



CubieCube* CubieCube::get_default(void) {
  CubieCube *res = new CubieCube[6];

  const Corners     cpU[CORNER_COUNT]  = { Corners::UBR, Corners::URF, Corners::UFL, Corners::ULB, Corners::DFR, Corners::DLF, Corners::DBL, Corners::DRB };
  const signed char  coU[CORNER_COUNT]  = { 0, 0, 0, 0, 0, 0, 0, 0 };
  const Edge       epU[EDGE_COUNT] = { Edge::UB, Edge::UR, Edge::UF, Edge::UL, Edge::DR, Edge::DF, Edge::DL, Edge::DB, Edge::FR, Edge::FL, Edge::BL, Edge::BR };
  const signed char  eoU[EDGE_COUNT] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    memcpy(res[0].cp, cpU, sizeof(cpU));
    memcpy(res[0].co, coU, sizeof(coU));
    memcpy(res[0].ep, epU, sizeof(epU));
    memcpy(res[0].eo, eoU, sizeof(eoU));

  const Corners     cpR[CORNER_COUNT]  = { Corners::DFR, Corners::UFL, Corners::ULB, Corners::URF, Corners::DRB, Corners::DLF, Corners::DBL, Corners::UBR };
  const signed char  coR[CORNER_COUNT]  = { 2, 0, 0, 1, 1, 0, 0, 2 };
  const Edge       epR[EDGE_COUNT] = { Edge::FR, Edge::UF, Edge::UL, Edge::UB, Edge::BR, Edge::DF, Edge::DL, Edge::DB, Edge::DR, Edge::FL, Edge::BL, Edge::UR };
  const signed char  eoR[EDGE_COUNT] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    memcpy(res[1].cp, cpR, sizeof(cpR));
    memcpy(res[1].co, coR, sizeof(coR));
    memcpy(res[1].ep, epR, sizeof(epR));
    memcpy(res[1].eo, eoR, sizeof(eoR));

  const Corners     cpF[CORNER_COUNT]  = { Corners::UFL, Corners::DLF, Corners::ULB, Corners::UBR, Corners::URF, Corners::DFR, Corners::DBL, Corners::DRB };
  const signed char  coF[CORNER_COUNT]  = { 1, 2, 0, 0, 2, 1, 0, 0 };
  const Edge       epF[EDGE_COUNT] = { Edge::UR, Edge::FL, Edge::UL, Edge::UB, Edge::DR, Edge::FR, Edge::DL, Edge::DB, Edge::UF, Edge::DF, Edge::BL, Edge::BR };
  const signed char  eoF[EDGE_COUNT] = { 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0 };
    memcpy(res[2].cp, cpF, sizeof(cpF));
    memcpy(res[2].co, coF, sizeof(coF));
    memcpy(res[2].ep, epF, sizeof(epF));
    memcpy(res[2].eo, eoF, sizeof(eoF));

  const Corners     cpD[CORNER_COUNT]  = { Corners::URF, Corners::UFL, Corners::ULB, Corners::UBR, Corners::DLF, Corners::DBL, Corners::DRB, Corners::DFR };
  const signed char  coD[CORNER_COUNT]  = { 0, 0, 0, 0, 0, 0, 0, 0 };
  const Edge       epD[EDGE_COUNT] = { Edge::UR, Edge::UF, Edge::UL, Edge::UB, Edge::DF, Edge::DL, Edge::DB, Edge::DR, Edge::FR, Edge::FL, Edge::BL, Edge::BR };
  const signed char  eoD[EDGE_COUNT] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    memcpy(res[3].cp, cpD, sizeof(cpD));
    memcpy(res[3].co, coD, sizeof(coD));
    memcpy(res[3].ep, epD, sizeof(epD));
    memcpy(res[3].eo, eoD, sizeof(eoD));

  const Corners     cpL[CORNER_COUNT]  = { Corners::URF, Corners::ULB, Corners::DBL, Corners::UBR, Corners::DFR, Corners::UFL, Corners::DLF, Corners::DRB };
  const signed char  coL[CORNER_COUNT]  = { 0, 1, 2, 0, 0, 2, 1, 0 };
  const Edge       epL[EDGE_COUNT] = { Edge::UR, Edge::UF, Edge::BL, Edge::UB, Edge::DR, Edge::DF, Edge::FL, Edge::DB, Edge::FR, Edge::UL, Edge::DL, Edge::BR };
  const signed char  eoL[EDGE_COUNT] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    memcpy(res[4].cp, cpL, sizeof(cpL));
    memcpy(res[4].co, coL, sizeof(coL));
    memcpy(res[4].ep, epL, sizeof(epL));
    memcpy(res[4].eo, eoL, sizeof(eoL));

  const Corners     cpB[CORNER_COUNT]  = { Corners::URF, Corners::UFL, Corners::UBR, Corners::DRB, Corners::DFR, Corners::DLF, Corners::ULB, Corners::DBL };
  const signed char  coB[CORNER_COUNT]  = { 0, 0, 1, 2, 0, 0, 2, 1 };
  const Edge       epB[EDGE_COUNT] = { Edge::UR, Edge::UF, Edge::UL, Edge::BR, Edge::DR, Edge::DF, Edge::DL, Edge::BL, Edge::FR, Edge::FL, Edge::UB, Edge::DB };
  const signed char  eoB[EDGE_COUNT] = { 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1 };
    memcpy(res[5].cp, cpB, sizeof(cpB));
    memcpy(res[5].co, coB, sizeof(coB));
    memcpy(res[5].ep, epB, sizeof(epB));
    memcpy(res[5].eo, eoB, sizeof(eoB));

  return res;
}

CubieCube CubieCube::get_cubiecube()
{
  CubieCube res;

  static const Corners   cp[CORNER_COUNT]   = { Corners::URF, Corners::UFL, Corners::ULB, Corners::UBR, Corners::DFR, Corners::DLF, Corners::DBL, Corners::DRB };
  static const signed char       co[CORNER_COUNT]   = { 0, 0, 0, 0, 0, 0, 0, 0 };
  static const Edge     ep[EDGE_COUNT]  = { Edge::UR, Edge::UF, Edge::UL, Edge::UB, Edge::DR, Edge::DF, Edge::DL, Edge::DB, Edge::FR, Edge::FL, Edge::BL, Edge::BR };
  static const signed char       eo[EDGE_COUNT]  = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    memcpy(res.cp, cp, sizeof(cp));
    memcpy(res.co, co, sizeof(co));
    memcpy(res.ep, ep, sizeof(ep));
    memcpy(res.eo, eo, sizeof(eo));

  return res;
}

int CubieCube::Cnk(int n, int k) const {
  int i, j, s;
  if (n < k)
    return 0;
  if (k > n / 2)
    k = n - k;
  for (s = 1, i = n, j = 1; i != n - k; i--, j++) {
    s *= i;
    s /= j;
  }
  return s;
}

void CubieCube::rotateLeft_corner(Corners* arr, int l, int r) const {
  Corners temp = arr[l];
  for (int i = l; i < r; i++)
      arr[i] = arr[i + 1];
  arr[r] = temp;
}

void CubieCube::rotateRight_corner(Corners* arr, int l, int r) const {
  Corners temp = arr[r];
  for (int i = r; i > l; i--)
    arr[i] = arr[i - 1];
  arr[l] = temp;
}


void CubieCube::rotateLeft_edge(Edge* arr, int l, int r) const {
  Edge temp = arr[l];
  for (int i = l; i < r; i++)
    arr[i] = arr[i + 1];
  arr[r] = temp;
}

void CubieCube::rotateRight_edge(Edge* arr, int l, int r) const {
  Edge temp = arr[r];
  for (int i = r; i > l; i--)
    arr[i] = arr[i - 1];
  arr[l] = temp;
}

void CubieCube::corner_multiply(const CubieCube &cp) {
    signed char oriA, oriB, ori;
    Corners cPerm[CORNER_COUNT];
    signed char cOri[CORNER_COUNT];
    for (int corn = 0; corn < CORNER_COUNT; ++corn) {
        cPerm[corn] = this->cp[static_cast<unsigned>(cp.cp[corn])];

        oriA = this->co[static_cast<unsigned>(cp.cp[corn])];
        oriB = cp.co[corn];
        ori = 0;

        if (oriA < 3 && oriB < 3) // if both cubes are regular cubes...
        {
            ori = oriA + oriB; // just do an addition modulo 3 here
            if (ori >= 3)
                ori -= 3; // the composition is a regular cube

            // not used in this implementation
        } else if (oriA < 3 && oriB >= 3) // if cube b is in a mirrored
        {
            ori = oriA + oriB;
            if (ori >= 6)
                ori -= 3; // the composition is a mirrored cube
        } else if (oriA >= 3 && oriB < 3) // if cube a is an a mirrored
        {
            ori = oriA - oriB;
            if (ori < 3)
                ori += 3; // the composition is a mirrored cube
        } else if (oriA >= 3 && oriB >= 3) // if both cubes are in mirrored
        {
            ori = oriA - oriB;
            if (ori < 0)
                ori += 3; // the composition is a regular cube
        }
        cOri[corn] = ori;
    }
    for(int corn = 0; corn < CORNER_COUNT; ++corn) {
        this->cp[corn] = cPerm[corn];
        this->co[corn] = cOri[corn];
    }
}

void CubieCube::edge_multiply(const CubieCube &cp) {
  Edge ePerm[EDGE_COUNT];
  signed char eOri[EDGE_COUNT] = {0};

  for(int edge = 0; edge < EDGE_COUNT; ++edge) {
    ePerm[edge] = this->ep[static_cast<unsigned>(cp.ep[edge])];
    eOri[edge] = (cp.eo[edge] + this->eo[static_cast<unsigned>(cp.ep[edge])]) % 2;
  }
  for(int edge = 0; edge < EDGE_COUNT; ++edge) {
    this->ep[edge] = ePerm[edge];
    this->eo[edge] = eOri[edge];
  }
}

void CubieCube::multiply(const CubieCube &cp)
{
  corner_multiply(cp);
  edge_multiply(cp);
}

short CubieCube::get_twist(void) const {
  short ret = 0;
  for (int i = static_cast<int>(Corners::URF); i < static_cast<int>(Corners::DRB); ++i)
    ret = static_cast<short>((3 * ret + this->co[i]));
  return ret;
}

void CubieCube::set_twist(short twist) {
  int twistParity = 0;
  for (int i = static_cast<int>(Corners::DRB) - 1; i >= static_cast<int>(Corners::URF); i--) {
    twistParity += this->co[i] = (signed char) (twist % 3);
    twist /= 3;
  }
  this->co[static_cast<int>(Corners::DRB)] = (signed char) ((3 - twistParity % 3) % 3);
}

short CubieCube::get_flip(void) const {
  short ret = 0;
  for (int i = static_cast<int>(Edge::UR); i < static_cast<int>(Edge::BR); ++i)
    ret = static_cast<short>((2 * ret + this->eo[i]));
  return ret;
}

void CubieCube::set_flip(short flip) {
  int flipParity = 0;
  for (int i = static_cast<int>(Edge::BR) - 1; i >= static_cast<int>(Edge::UR); --i) {
      flipParity += this->eo[i] = (signed char) (flip % 2);
      flip /= 2;
  }
  this->eo[static_cast<int>(Edge::BR)] = (signed char) ((2 - flipParity % 2) % 2);
}

short CubieCube::corner_parity(void) const {
  int s = 0;
  for (int i = static_cast<int>(Corners::DRB); i >= static_cast<int>(Corners::URF) + 1; --i)
    for (int j = i - 1; j >= static_cast<int>(Corners::URF); --j)
      if (this->cp[j] > this->cp[i])
        s++;
  return static_cast<short>((s % 2));
}

short CubieCube::edge_parity(void) const {
  int s = 0;
  for (int i = static_cast<int>(Edge::BR); i >= static_cast<int>(Edge::UR) + 1; --i)
    for (int j = i - 1; j >= static_cast<int>(Edge::UR); j--)
      if (this->ep[j] > this->ep[i])
        s++;
  return static_cast<short>((s % 2));
}

short CubieCube::get_FRtoBR(void) const {
  int a = 0;
  int x = 0;
  int b = 0;
  Edge edge4[4];
  // compute the index a < (12 choose 4) and the permutation array perm.
  for (int j = static_cast<int>(Edge::BR); j >= static_cast<int>(Edge::UR); j--)
    if (static_cast<int>(Edge::FR) <= static_cast<unsigned>(this->ep[j]) &&
        static_cast<unsigned>(this->ep[j]) <= static_cast<int>(Edge::BR)) {
      a += Cnk(11 - j, x + 1);
      edge4[3 - x++] = this->ep[j];
    }

  for (int j = 3; j > 0; j--) {
    int k = 0;
    while (static_cast<int>(edge4[j]) != j + 8) {
      rotateLeft_edge(edge4, 0, j);
      k++;
    }
    b = (j + 1) * b + k;
  }
  return static_cast<short>((24 * a + b));
}
void CubieCube::set_FRtoBR(short idx) {
  int x;
  int k;
  Edge sliceEdge[4] = { Edge::FR, Edge::FL, Edge::BL, Edge::BR };
  Edge otherEdge[8] = { Edge::UR, Edge::UF, Edge::UL, Edge::UB, Edge::DR, Edge::DF, Edge::DL, Edge::DB };
  int b = idx % 24; // Permutation
  int a = idx / 24; // Combination

  // invalidate all edges
  for (int e = 0; e < EDGE_COUNT; ++e)
    this->ep[e] = Edge::DB;

  // generate permutation from index b
  for (int j = 1; j < 4; j++)
  {
    k = b % (j + 1);
    b /= j + 1;
    while (k-- > 0)
      rotateRight_edge(sliceEdge, 0, j);
  }

  x = 3;
  // generate combination and set slice edges
  for (int j = static_cast<int>(Edge::UR); j <= static_cast<int>(Edge::BR); ++j)
    if (a - Cnk(11 - j, x + 1) >= 0) {
      this->ep[j] = sliceEdge[3 - x];
      a -= Cnk(11 - j, x-- + 1);
    }

  x = 0;
  // set the remaining edges UR..DB
  for (int j = static_cast<int>(Edge::UR); j <= static_cast<int>(Edge::BR); ++j)
    if (this->ep[j] == Edge::DB)
      this->ep[j] = otherEdge[x++];
}

short CubieCube::get_URFtoDLF(void) const {
  int a = 0;
  int x = 0;
  int b = 0;
  Corners corner6[6];

  // compute the index a < (8 choose 6) and the corner permutation.
  for (int j = static_cast<int>(Corners::URF); j <= static_cast<int>(Corners::DRB); j++)
    if (this->cp[j] <= Corners::DLF) {
      a += Cnk(j, x + 1);
      corner6[x++] = this->cp[j];
    }

  // compute the index b < 6! for the
  for (int j = 5; j > 0; j--)
  {
    int k = 0;
    while (corner6[j] != static_cast<Corners>(j)) {
      rotateLeft_corner(corner6, 0, j);
      k++;
    }
    b = (j + 1) * b + k;
  }
  return static_cast<short>(720 * a + b);
}

void CubieCube::set_URFtoDLF(short idx) {
  int x;
  Corners corner6[6] = { Corners::URF, Corners::UFL, Corners::ULB, Corners::UBR, Corners::DFR, Corners::DLF };
  Corners otherCorner[2] = { Corners::DBL, Corners::DRB };
  int b = idx % 720; // Permutation
  int a = idx / 720; // Combination
  int k;

  // invalidate all corners
  for(int c = 0; c < CORNER_COUNT; c++)
      this->cp[c] = Corners::DRB;

  // generate permutation from index b
  for (int j = 1; j < 6; j++) {
    k = b % (j + 1);
    b /= j + 1;
    while (k-- > 0)
        rotateRight_corner(corner6, 0, j);
  }
  x = 5;
  // generate combination and set corners
  for (int j = static_cast<int>(Corners::DRB); j >= 0; j--)
    if (a - Cnk(j, x + 1) >= 0) {
      this->cp[j] = corner6[x];
      a -= Cnk(j, x-- + 1);
    }

  x = 0;
  for (int j = static_cast<int>(Corners::URF); j <= static_cast<int>(Corners::DRB); j++)
    if (this->cp[j] == Corners::DRB)
      this->cp[j] = otherCorner[x++];
}

int CubieCube::get_URtoDF(void) const {
  int a = 0;
  int x = 0;
  int b = 0;
  Edge edge6[6];

  // compute the index a < (12 choose 6) and the edge permutation.
  for (int j = static_cast<int>(Edge::UR); j <= static_cast<int>(Edge::BR); j++)
    if (this->ep[j] <= Edge::DF) {
      a += Cnk(j, x + 1);
      edge6[x++] = this->ep[j];
    }

  // compute the index b < 6! for the
  for (int j = 5; j > 0; j--) {
    int k = 0;
    while (edge6[j] != static_cast<Edge>(j)) {
      rotateLeft_edge(edge6, 0, j);
      k++;
    }
    b = (j + 1) * b + k;
  }
  return 720 * a + b;
}

void CubieCube::set_URtoDF(int idx) {
  int x;
  int k;
  Edge edge6[6] = { Edge::UR, Edge::UF, Edge::UL, Edge::UB, Edge::DR, Edge::DF };
  Edge otherEdge[6] = { Edge::DL, Edge::DB, Edge::FR, Edge::FL, Edge::BL, Edge::BR };
  int b = idx % 720; // Permutation
  int a = idx / 720; // Combination

  // Use BR to invalidate all edges
  for(int e = 0; e < EDGE_COUNT; e++)
    this->ep[e] = Edge::BR;

  // generate permutation from index b
  for (int j = 1; j < 6; j++)
  {
    k = b % (j + 1);
    b /= j + 1;
    while (k-- > 0)
      rotateRight_edge(edge6, 0, j);
  }

  // generate combination and set edges
  x = 5;
  for (int j = static_cast<int>(Edge::BR); j >= 0; j--)
    if (a - Cnk(j, x + 1) >= 0) {
      this->ep[j] = edge6[x];
      a -= Cnk(j, x-- + 1);
    }

  x = 0;
  // set the remaining edges DL..BR
  for (int j = static_cast<int>(Edge::UR); j <= static_cast<int>(Edge::BR); ++j)
    if (this->ep[j] == Edge::BR)
      this->ep[j] = otherEdge[x++];
}

short CubieCube::get_URtoUL(void) const {
    int a = 0;
    int b = 0;
    int x = 0;
    Edge edge3[3];

    // compute the index a < (12 choose 3) and the edge permutation.
    for (int j = static_cast<int>(Edge::UR); j <= static_cast<int>(Edge::BR); j++)
      if (this->ep[j] <= Edge::UL) {
        a += Cnk(j, x + 1);
        edge3[x++] = this->ep[j];
      }

    // compute the index b < 3! for the
    for (int j = 2; j > 0; j--) {
      int k = 0;
      while (edge3[j] != static_cast<Edge>(j)) {
        rotateLeft_edge(edge3, 0, j);
        k++;
      }
      b = (j + 1) * b + k;
    }
    return static_cast<short>(6 * a + b);
}

void CubieCube::set_URtoUL(short idx) {
    int x;
    int e;
    int k;
    Edge edge3[3] = { Edge::UR, Edge::UF, Edge::UL };
    int b = idx % 6; // Permutation
    int a = idx / 6; // Combination

    // invalidate all edges
    for(e = 0; e < EDGE_COUNT; e++) {
        this->ep[e] = Edge::BR;
    }

    // generate permutation from index b
    for (int j = 1; j < 3; j++) {
      k = b % (j + 1);
      b /= j + 1;
      while (k-- > 0)
        rotateRight_edge(edge3, 0, j);
    }

    x = 2;
    // generate combination and set edges
    for (int j = static_cast<int>(Edge::BR); j >= 0; j--) {
        if (a - Cnk(j, x + 1) >= 0) {
            this->ep[j] = edge3[x];
            a -= Cnk(j, x-- + 1);
        }
    }
}

short CubieCube::get_UBtoDF(void) const {
    int a = 0;
    int x = 0;
    int b = 0;
    Edge edge3[3];

    // compute the index a < (12 choose 3) and the edge permutation.
    for (int j = static_cast<int>(Edge::UR); j <= static_cast<int>(Edge::BR); j++)
      if (Edge::UB <= this->ep[j] && this->ep[j] <= Edge::DF) {
        a += Cnk(j, x + 1);
        edge3[x++] = this->ep[j];
      }

    // compute the index b < 3! for the
    for (int j = 2; j > 0; j--) {
      int k = 0;
      while (edge3[j] != static_cast<Edge>(static_cast<int>(Edge::UB) + j)) {
        rotateLeft_edge(edge3, 0, j);
        k++;
      }
      b = (j + 1) * b + k;
    }
    return static_cast<short>(6 * a + b);
}

void CubieCube::set_UBtoDF(short idx) {
  int x;
  int e;
  int k;
  Edge edge3[3] = { Edge::UB, Edge::DR, Edge::DF };
  int b = idx % 6; // Permutation
  int a = idx / 6; // Combination

  // invalidate all edges
  for (e = 0; e < EDGE_COUNT; e++)
    this->ep[e] = Edge::BR;

  // generate permutation from index b
  for (int j = 1; j < 3; j++)
  {
    k = b % (j + 1);
    b /= j + 1;
    while (k-- > 0)
      rotateRight_edge(edge3, 0, j);
  }
  x = 2;
  // generate combination and set edges
  for (int j = static_cast<int>(Edge::BR); j >= 0; j--)
    if (a - Cnk(j, x + 1) >= 0) {
      this->ep[j] = edge3[x];
      a -= Cnk(j, x-- + 1);
    }
}

int CubieCube::get_URFtoDLB(void) const {
  Corners perm[CORNER_COUNT];
  int b = 0;

  for (int i = 0; i < 8; i++)
    perm[i] = this->cp[i];

  for (int j = 7; j > 0; j--)// compute the index b < 8! for the permutation in perm
  {
    int k = 0;
    while (perm[j] != static_cast<Corners>(j)) {
      rotateLeft_corner(perm, 0, j);
      k++;
    }
    b = (j + 1) * b + k;
  }
  return b;
}

void CubieCube::set_URFtoDLB(int idx) {
  Corners perm[CORNER_COUNT] = { Corners::URF, Corners::UFL, Corners::ULB, Corners::UBR, Corners::DFR, Corners::DLF, Corners::DBL, Corners::DRB };
  int k;
  int x = 7;// set corners

  for (int j = 1; j < 8; j++) {
    k = idx % (j + 1);
    idx /= j + 1;
    while (k-- > 0)
      rotateRight_corner(perm, 0, j);
  }

  for (int j = 7; j >= 0; j--)
    this->cp[j] = perm[x--];
}

int CubieCube::get_URtoBR(void) const {
  Edge perm[EDGE_COUNT];
  int b = 0;
  int i;

  for (i = 0; i < 12; i++)
    perm[i] = this->ep[i];

  // compute the index b < 12! for the permutation in perm
  for (int j = 11; j > 0; j--)
  {
    int k = 0;
    while (perm[j] != static_cast<Edge>(j)) {
      rotateLeft_edge(perm, 0, j);
      k++;
    }
    b = (j + 1) * b + k;
  }
  return b;
}

void CubieCube::set_URtoBR(int idx) {
  Edge perm[EDGE_COUNT] = { Edge::UR, Edge::UF, Edge::UL, Edge::UB, Edge::DR, Edge::DF, Edge::DL, Edge::DB, Edge::FR, Edge::FL, Edge::BL, Edge::BR };
  int k;
  int x = 11;

  for (int j = 1; j < 12; j++) {
    k = idx % (j + 1);
    idx /= j + 1;
    while (k-- > 0)
      rotateRight_edge(perm, 0, j);
  }

  for (int j = 11; j >= 0; j--)
    this->ep[j] = perm[x--];
}

int CubieCube::get_URtoDF_standalone(short idx1, short idx2) {
  CubieCube a = CubieCube::get_cubiecube();
  CubieCube b = CubieCube::get_cubiecube();
  a.set_URtoUL(idx1);
  b.set_UBtoDF(idx2);

  for (int i = 0; i < 8; i++) {
    if (a.ep[i] != Edge::BR) {

      // collision
      if (b.ep[i] != Edge::BR) {
        return -1;
      } else {
        b.ep[i] = a.ep[i];
      }
    }
  }

  return b.get_URtoDF();
}
